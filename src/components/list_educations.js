import React from "react"
import SingleEducation from "./single_education";


class ListEducation extends React.Component {

    constructor(props) {
        super(props);
        this.educations = this.props.educations
    }

    render() {
        let {search_value} = this.props
        if (search_value.length > 80) {
            search_value = "Free Music"
        }
        return (
            <>
               <div>
                    {this.educations.map(item => <SingleEducation single_video={item} key={Math.random()}/>)
                    }
                </div>
            </>
        )
    }

}

export default ListEducation