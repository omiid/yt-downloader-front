import React from "react"
import {Helmet} from "react-helmet";
import SingleMovie from "./single_movie";


class ListMovie extends React.Component {

    constructor(props) {
        super(props);
        this.movies = this.props.movies
    }

    render() {
        let {search_value} = this.props
        if (search_value.length > 80) {
            search_value = "Free Music"
        }
        return (
            <>
                <Helmet>
                    <title>Download Free movies series tv shows {search_value} | televisiun.com </title>
                    <meta name="description"
                          content="download free movies, series, tvshows, documentaries"/>
                    <meta name="keywords"
                          content="free,download,movie,film,series"/>
                </Helmet>
                <div>
                    {this.movies.map(item => <SingleMovie key={Math.random()} single_video={item}/>)
                    }
                </div>
            </>
        )
    }

}

export default ListMovie