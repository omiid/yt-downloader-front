import React from "react"
import SingleMusic from "./single_music";
import MusicProvider from "../utils/MusicProvider";
import MusicPlayer from "./music_player";
import {Helmet} from "react-helmet";


class ListMusic extends React.Component {

    constructor(props) {
        super(props);
        this.musics = this.props.musics
        this.BASE_PLAYING = []
        this.props.musics.map(_ =>
            this.BASE_PLAYING.push(false)
        )

        this.state = {
            playing: this.BASE_PLAYING,
            music: null
        }
    }

    handlePlaying(e, index) {
        this.setState({music: this.musics[index]})
    }


    render() {
        let {search_value} = this.props
        if (search_value.length > 80) {
            search_value = "Free Music"
        }
        return (
            <>
                <Helmet>
                    <title>Download Free MP3 Music {search_value} | televisiun.com </title>
                    <meta lang="en" name="description"
                          content="Mp3 Download free, easily and fast. Find your favorite songs and listen to them offline. The latest music hits, high quality mp3.
                          320Kbps Free music download | Listen audio music online | Download songs on mobile.
                          [Safe, No virus, No plugins]
                          "/>

                    <meta name="keywords"
                          content="free,mp3,download,music,songs,online,listen,search,sound,audio,remix"/>
                    <meta lang="fa" name="keywords"
                          content="Persian Music, Iran Music, Download Persian Music, Free Iran Music, Iranian Music, Radio Javan, RadioJavan, Irani, Irani MP3s, Irani Free Download, Persian MP3s, Iran MP3s, Download Persian MP3s, Download Persian Free Music, Download Iran MP3s, Download Iran Music"/>
                </Helmet>

                <MusicProvider>
                    <div>
                        {this.musics.map((item, index) =>
                            <SingleMusic
                                single_music={item}
                                key={Math.random()}
                                index={index}
                            />
                        )}
                    </div>
                    <MusicPlayer musics={this.musics}/>
                </MusicProvider>
            </>)
    }

}

export default ListMusic