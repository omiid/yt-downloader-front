import React from "react"
import {Helmet} from "react-helmet";
import SingleYoutube from "./single_youtube";


class ListYoutubes extends React.Component {

    constructor(props) {
        super(props);
        this.youtubes = this.props.youtubes
    }

    render() {
        let {search_value} = this.props
        if (search_value.length > 80) {
            search_value = "Free Music"
        }
        return (
            <>
                <Helmet>
                    <title>Download and convert Free youtube mp3 {search_value} | televisiun.com </title>
                    <meta name="description"
                          content="Convert and download youtube videos to mp3 (audio) or mp4 (video) files for free. There is no registration or software needed.
                          "/>
                    <meta name="keywords"
                          content="youtube,mp3,converter,downloader,free"/>
                </Helmet>
                <div>
                    {this.youtubes.map(item => <SingleYoutube key={Math.random()} single_video={item}/>)
                    }
                </div>
            </>
        )
    }

}

export default ListYoutubes