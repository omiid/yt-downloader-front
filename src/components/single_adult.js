import React from "react"
import DropSVG from "../assets/icons/dropSVG";
import Mp4 from "../assets/icons/mp4SVG";
import API from "../utils/API";
import DownloadLinkModal from "./download_links";

class SingleAdult extends React.Component {

    constructor(props) {
        super(props);
        this.node = React.createRef();
        this.state = {
            downloadVideoOpen: false,
            downloadAudioOpen: false,
            isDownloadLinkOpen: null,
        }
        this.toggleDownloadAudioOpen = this.toggleDownloadAudioOpen.bind(this)
        this.toggleDownloadLinks = this.toggleDownloadLinks.bind(this)
        this.url = this.props.single_video.url
    }

    componentDidMount() {
        document.addEventListener('mousedown', this.handleClickOutside);
    }

    componentWillUnmount() {
        document.removeEventListener('mousedown', this.handleClickOutside);
    }

    render() {
        let {isDownloadLinkOpen} = this.state

        const {single_video} = this.props
        const {
            name,
            description,
            image,
            url,
            download_links,
        } = single_video
        const downloads = {}
        const quick_accesses = []

        let dataEncodings = JSON.parse(download_links[0])

        function getDownloadUrl(quality) {
            let url_1 = ''
            let findMp4 = dataEncodings.find(function (encoding) {
                return encoding.filename.indexOf('_hls') === -1 && encoding.quality === quality;
            });
            if (findMp4) {
                url_1 = findMp4.filename;
            } else {
                let downloadEncodings = dataEncodings.filter(function (encoding) {
                    return encoding.filename.indexOf('_hls') === -1 && encoding.quality === 360;
                });
                if (downloadEncodings.length) {
                    url_1 = downloadEncodings[0].filename;
                } else {
                    downloadEncodings = dataEncodings.filter(function (encoding) {
                        return encoding.filename.indexOf('_hls') === -1;
                    });
                    if (downloadEncodings.length) {
                        url_1 = downloadEncodings[0].filename;
                    }
                }
            }
            return url_1 + '&d=1';
        }

        const download_qualities = [240, 360, 480, 720, 1080]
        download_qualities.map(quality => {
            let download_link = getDownloadUrl(quality)
            download_link = download_link.replaceAll('amp;', '')
            if (!(quality in downloads)) {
                downloads[quality] = [{
                    link: "http:" + download_link,
                    title: quality.toString() + "p - " + name
                }]
                if ([1080, 720, 480].includes(quality)) {
                    quick_accesses.push({
                        quality: quality,
                        link: "http:" + download_link
                    })
                }
            } else {
                downloads[quality].push({
                    link: "http:" + download_link,
                    title: quality.toString() + "p - " + name
                })
            }
            return true;
        })

        return (
            <>
                <DownloadLinkModal
                    name={name}
                    downloads={downloads}
                    download_links={Object.keys(downloads)}
                    isOpen={isDownloadLinkOpen}
                    close={this.toggleDownloadLinks}/>

                <div className="sv-parent" key={Math.random()}>
                    <div className="sv-thumbnail-adult" onClick={(_) => window.open(url)}>
                        {/*<VideoSVG/>*/}
                        <img alt="YTS DOWNLOADER" src={image} className="sv-thumbnail-image"/>
                        <div className="sv-video-duration">
                            {"duration"}
                        </div>
                    </div>
                    <div className="sv-detail" onClick={(_) => window.open(url)}>
                        <a href={url} target="_blank" rel="noopener noreferrer"
                           className="sv-title">{name}</a>
                        <div className="sv-metadata-line">
                            {/*<div className="sv-owner">{genres.join(', ')}</div>*/}
                        </div>
                        <div className="sv-description">{description}</div>
                    </div>
                    <div className="sv-download-link">
                        {quick_accesses.length > 0 &&
                        <div key={Math.random()} className="sv-download-video">
                            {quick_accesses.map(quick_access =>
                                <div key={Math.random()} className="sv-quick-download"
                                     onClick={(_) => window.open(quick_access['link'])}>
                                    <Mp4 className="sv-quick-download-icon"/>
                                    <span className="sv-quick-download-icon-text">
                            {quick_access['quality']}</span>
                                </div>)}
                        </div>}
                        <div className="sv-download-audio" onClick={this.toggleDownloadLinks}>
                            Other Formats
                            <DropSVG className="sv-dropdown-svg"/>
                        </div>
                        {/*{*/}
                        {/*    <div id="sv-download-audio-options" className={this.state.downloadAudioOptions}>*/}
                        {/*    </div>*/}
                        {/*}*/}
                    </div>
                </div>
            </>

        );
    }


    toggleDownloadLinks() {
        let downloadLink = this.state.isDownloadLinkOpen
        this.setState({isDownloadLinkOpen: !downloadLink})
    }

    toggleDownloadAudioOpen() {

        if (this.state.downloadAudioOptions === '') {
            let search_data = JSON.stringify({video_link: this.url})
            API.post("download_links", search_data, {
                headers: {
                    "Content-Type": "application/json",
                },
            })
                .then((res) => {

                    this.setState({
                        urls: res.data.data,
                        downloadAudioOptions: 'sv-download-audio-options'
                    })
                })

        } else {
            this.setState({
                downloadAudioOptions: '',
            })
        }
    }
}

export default SingleAdult