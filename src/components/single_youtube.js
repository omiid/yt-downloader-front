import React from "react"
import VideoSVG from "../assets/icons/videoSVG";
import DropSVG from "../assets/icons/dropSVG";
import Mp3 from "../assets/icons/mp3SVG";
import Mp4 from "../assets/icons/mp4SVG";
import API from "../utils/API";
import DownloadLinkModal from "./download_links";

class SingleYoutube extends React.Component {

    constructor(props) {
        super(props);
        this.node = React.createRef();
        this.all_qualities = [
            {
                title: '480P',
                all_movies: [
                    {
                        'title': 'The Shawshank Redemption Ganool Encode',
                        'main_language': 'English',
                        'link': 'https://kosshertv.com'
                    },
                    {
                        'title': 'The Shawshank Redemption MKVCage',
                        'main_language': 'Farsi',
                        'link': 'https://kosshertv.mamadeshan.com'
                    },
                    {
                        'title': 'The Shawshank Redemption PLAXA',
                        'main_language': 'Pashto',
                        'link': 'https://kosshertv.com'
                    },
                    {'title': 'The Shawshank Redemption BOB', 'main_language': 'Ordu', 'link': 'https://kosshertv.com'},
                ]
            }, {
                title: '720P',
                all_movies: [
                    {'title': 'The Shawshank Redemption', 'main_language': 'English', 'link': 'https://kosshertv.com'},
                    {'title': 'The Raw', 'main_language': 'Farsi', 'link': 'https://kosshertv.mamadeshan.com'},
                    {'title': 'The Black Hawk Down', 'main_language': 'Pashto', 'link': 'https://kosshertv.com'},
                    {
                        'title': 'Everything Happen For a Reason',
                        'main_language': 'Ordu',
                        'link': 'https://kosshertv.com'
                    },
                ]
            },
        ];
        this.qualitiesClassName_ = this.all_qualities.map(_ => 'sv-modal-quality')
        this.state = {
            downloadVideoOpen: false,
            downloadAudioOpen: false,
            formatsIsLoading: false,
            isDownloadLinkOpen: false,
            downloadFormatOptions: '',
            qualitiesClassName: this.qualitiesClassName_
        }
        this.handleClickOutside = this.handleClickOutside.bind(this)
        this.toggleDownloadAudioOpen = this.toggleDownloadAudioOpen.bind(this)
        this.toggleDownloadLinks = this.toggleDownloadLinks.bind(this)
        this.toggleTab = this.toggleTab.bind(this)
        this.url = this.props.single_video.url
    }

    componentDidMount() {
        document.addEventListener('mousedown', this.handleClickOutside);
    }

    componentWillUnmount() {
        document.removeEventListener('mousedown', this.handleClickOutside);
    }

    render() {
        let {isDownloadLinkOpen, downloadFormatOptions, formatsIsLoading} = this.state

        const {single_video} = this.props
        const {
            published_time_text,
            video_duration,
            view_count_text,
            owner_text,
            video_id,
            url,
            image,
            title,
            description,
        } = single_video
        const downloads = {}
        const quick_accesses = []
        if (downloadFormatOptions !== '') {
            downloadFormatOptions.map(download_link => {
                if (!(download_link['extension'] in downloads)) {
                    downloads[download_link['extension']] = [{
                        title: download_link['format'],
                        link: download_link['url']
                    }]
                    if (['1080p', '720p', '480p'].includes(download_link['format'])) {
                        quick_accesses.push({quality: download_link['extension'], link: download_link['url']})
                    }
                } else {
                    downloads[download_link['extension']].push({
                        title: download_link['format'],
                        link: download_link['url']
                    })
                }
            })
        }
        return (
            <>
                {downloadFormatOptions !== '' && <DownloadLinkModal
                    name={title}
                    downloads={downloads}
                    download_links={downloadFormatOptions}
                    isOpen={isDownloadLinkOpen}
                    close={this.toggleDownloadLinks}/>}
                <div className="sv-parent">

                    <div className="sv-thumbnail-youtube" onClick={(_) => window.open(url, "_blank")}>
                        {/*<VideoSVG/>*/}
                        <img alt="YTS DOWNLOADER" src={image} className="sv-thumbnail-image"/>
                        <div className="sv-video-duration">
                            {video_duration}
                        </div>
                    </div>
                    <div className="sv-detail" onClick={(_) => window.open(url, "_blank")}>
                        <a href={url} target="_blank" rel="noopener noreferrer" className="sv-title-youtube">{title}</a>
                        <div className="sv-metadata-line">
                            <div className="sv-views">{view_count_text}</div>
                            <div className="sv-published-date">{published_time_text}</div>
                        </div>
                        <div className="sv-owner">{owner_text}</div>
                        <div className="sv-description">{description}</div>
                    </div>
                    <div className="sv-download-link">
                        <div className="sv-download-video">
                            <div className="sv-quick-download" onClick={this.getMP3}>
                                <Mp3 className="sv-quick-download-icon"/>
                                <span className="sv-quick-download-icon-text">
                            MP3</span>
                            </div>
                            <div className="sv-quick-download" onClick={(_) => this.toggleDownloadAudioOpen("360p")}>
                                <Mp4 className="sv-quick-download-icon"/>
                                <span className="sv-quick-download-icon-text">
                            360p</span>
                            </div>
                            <div className="sv-quick-download" onClick={(_) => this.toggleDownloadAudioOpen("480p")}>
                                <Mp4 className="sv-quick-download-icon"/>
                                <span className="sv-quick-download-icon-text">
                            480p</span>
                            </div>
                        </div>
                        <div className="sv-download-audio" onClick={this.toggleDownloadLinks}>
                            {formatsIsLoading ? <div className="loading">Loading Formats</div> :
                                "Other Formats"}
                            {!formatsIsLoading ?
                                <DropSVG className="sv-dropdown-svg"/> : null}
                        </div>
                    </div>
                </div>
            </>
        );
    }

    handleClickOutside(e) {
        if (e.target.className === "sv-modal-overlay") {
            this.toggleDownloadLinks();
        }
    }

    toggleTab(index) {
        let qualitiesClassName_ = JSON.parse(JSON.stringify(this.qualitiesClassName_))
        qualitiesClassName_[index] = this.state.qualitiesClassName[index] === 'sv-modal-quality active' ? 'sv-modal-quality' : 'sv-modal-quality active'
        this.setState({qualitiesClassName: qualitiesClassName_})
    }

    toggleDownloadLinks() {
        this.toggleDownloadAudioOpen()
        let downloadLink = this.state.isDownloadLinkOpen
        this.setState({isDownloadLinkOpen: !downloadLink})
    }

    getMP3 = (_) => {
        let search_data = JSON.stringify({video_link: this.url, format_id: "mp3"})
        API.post("download_links", search_data, {
            headers: {
                "Content-Type": "application/json",
            },
        })
            .then((res) => {
                window.open(res.data.data.formats['mp3'][0]['url'], "_self");
            })

    }

    toggleDownloadAudioOpen(format_id = '') {
        if (this.state.downloadFormatOptions === '') {
            this.setState({formatsIsLoading: true})
            let search_data = JSON.stringify({video_link: this.url})
            API.post("download_links", search_data, {
                headers: {
                    "Content-Type": "application/json",
                },
            })
                .then((res) => {
                    this.setState({
                        downloadFormatOptions: res.data.data.formats[0], formatsIsLoading: false
                    })
                    let all_formats = this.state.downloadFormatOptions
                    if (format_id !== '') {
                        all_formats.map(single_format => {
                            if (single_format['format'].includes(format_id)) {
                                window.open(single_format['url'], "_blank");
                                return;
                            }
                        })
                    }
                })

        } else if (format_id !== '') {
            let all_formats = this.state.downloadFormatOptions
            all_formats.map(single_format => {
                if (single_format['format'].includes(format_id)) {
                    window.open(single_format['url'], "_blank");
                    return;
                }
            })
        }

    }
}

export default SingleYoutube